from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model

User = get_user_model()


class FollowerRelation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) ##owner
    followers = models.ManyToManyField(User, 
                                        related_name='following',
                                        blank=True, 
                                        through='FollowerRelation')
    location = models.CharField(max_length=250, null=True, blank=True)
    bio = models.TextField( null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


def signal_for_profile(sender, instance, created,  **kwargs):
    if created:
        Profile.objects.create(user=instance)

post_save.connect(signal_for_profile, sender=User)
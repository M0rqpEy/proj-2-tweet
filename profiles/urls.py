from django.urls import path
from .views import profile_detail_view, profile_update_view

app_name='profile'

urlpatterns = [
    path('<str:username>/edit/',  profile_update_view, name='update'),
    path('<str:username>/detail/',  profile_detail_view, name='detail'),
]

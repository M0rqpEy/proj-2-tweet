from django.core.exceptions import PermissionDenied
from django.shortcuts import render,  get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.urls import reverse

from .models import Profile
from .forms import ProfileForm



def profile_detail_view(request, username):
    profile_obj = get_object_or_404(Profile, user__username=username)
    context = {
        'username':username,
        'profile': profile_obj
    }
    return render(request, 'profiles/detail.html', context)

@login_required
def profile_update_view(request,username, **kwargs):
    if str(request.user) != username:
        raise PermissionDenied
    profile_obj = get_object_or_404(Profile, user__username=username)
    form = ProfileForm(request.POST or None, 
                    instance=profile_obj,
                    initial={
                        'first_name':request.user.first_name,
                        'last_name':request.user.last_name,
                        'email':request.user.email,
                    })
    if form.is_valid():
        request.user.first_name = form.cleaned_data.get('first_name')
        request.user.last_name = form.cleaned_data.get('last_name')
        request.user.email = form.cleaned_data.get('email')
        form.save()
        request.user.save()
        return redirect(reverse('profile:detail', args=[username]))
    context = {
        'form':form,
        'btn_label': 'Save',
        'title': 'Update Profile',
    }
    return render(request, 'profiles/update_form.html', context)
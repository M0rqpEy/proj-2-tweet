from django.urls import path 

from .views import (
   profile_follow_action,
   profiles_view,
   profile_detail_api_view,

    )

urlpatterns = [
    path('<str:username>/follow/', profile_follow_action),
    path('<str:username>/', profile_detail_api_view),
    path('', profiles_view),
]

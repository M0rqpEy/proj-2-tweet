from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.http import is_safe_url
from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from ..models import Profile
from .serializers import PublicProfileSerializer

USER = get_user_model()
ALLOWED_HOSTS = settings.ALLOWED_HOSTS



@api_view(['GET','POST'])
@permission_classes([IsAuthenticated])
def profile_follow_action(request, username, *args, **kwargs):
    '''
    action: follow or unfollow
    '''
    try:
        profile = get_object_or_404(Profile, user__username=username)
    except :
        return Response({'detail': 'This profile is not exist'}, status=400) 
    if request.data: # POST
        if str(request.user) == username:
           return Response({'detail': 'Gatcha, bich'}, status=400)
        action = request.data.get('action')
        if action == 'follow':
            profile.followers.add(request.user)
            return Response({'detail': f'U are following on profile {profile.user}'}, status=200) 
        elif action == 'unfollow':
            profile.followers.remove(request.user)
            return Response({'detail': f'U are unfollowing on profile {profile.user}'}, status=200) 
        else:
            return Response({'detail': 'Incorrect data'}, status=400) 
    # GET
    current_followers = profile.followers.all()
    return Response({'Count folowers': current_followers.count()}, status=200) 


@api_view(['GET'])
def profiles_view(request):
    qs = Profile.objects.all().select_related('user').prefetch_related(
                                                        'followers', 
                                                        'user__following'
                                                    )
    serializer = PublicProfileSerializer(qs, many=True, context={'request': request})
    return Response(serializer.data, status=200)


@api_view(['GET'])
def profile_detail_api_view(request, username):
    profile_obj = Profile.objects.filter(
                                    user__username=username
                                ).select_related(
                                    'user',
                                ).prefetch_related(
                                    'user__following',
                                    'followers',
                                )
    if not profile_obj.exists():
        return Response({"detail":f"User {username} not found"}, status=404)
    profile_obj = profile_obj.first()
    serializer = PublicProfileSerializer(profile_obj, context={'request': request}) 
    return Response(serializer.data, status=200)
from django.contrib import admin
from .models import Profile, FollowerRelation


class FollowerRelationAdmin(admin.TabularInline):
    model = FollowerRelation

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    inlines = [FollowerRelationAdmin]
    list_display = ['id', 'user']
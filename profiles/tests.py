from django.test import TestCase
from django.contrib.auth import get_user_model

from .models import Profile

User = get_user_model()

class ProfileTestCase(TestCase):
    
    def setUp(self):
        self.user1 = User.objects.create_user(username='test1', password='test1')
        self.user2 = User.objects.create_user(username='test2', password='test2')

    def test_profile_created(self):
        user1 = self.user1
        user1_profile = Profile.objects.get(user=user1)
        self.assertEqual(user1_profile.user, user1)
        self.assertEqual(Profile.objects.count(), 2)

    def test_follow_action(self):
        user1 = self.user1
        user2 = self.user2
        user1_profile = Profile.objects.get(user=user1)
        user1_profile.followers.add(user2)
        self.assertEqual(user1.following.count(), 0)
        self.assertEqual(user2.following.count(), 1)




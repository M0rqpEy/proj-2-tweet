from django.shortcuts import render, redirect 
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from .forms import  MyUserCreationForm



LOGOUT_REDIRECT_URL = settings.LOGOUT_REDIRECT_URL
LOGIN_REDIRECT_URL = settings.LOGIN_REDIRECT_URL


def login_view(request):
    form = AuthenticationForm(request, data=request.POST or None)
    if form.is_valid():
        user_ = form.get_user()
        login(request, user_)
        return redirect(reverse_lazy(LOGIN_REDIRECT_URL))
    context = {
        "form": form,
        'btn_label': 'Login',
        'title': 'Login'
    }
    return render(request, 'accounts/auth.html', context)

@login_required
def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect(reverse_lazy(LOGOUT_REDIRECT_URL))
    context = {
        "form": None,
        'btn_label': 'Logout',
        'title': 'Logout',
        'description': 'U wanna logout? Noooooooooo',
    }
    return render(request, 'accounts/auth.html', context)

def register_view(request):
    form = MyUserCreationForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        # user = form.save(commit=False)
        # user.is_active = False
        # user.save()
        user.set_password(form.cleaned_data.get('password1'))
        return redirect(reverse_lazy('login'))
    context = {
        "form": form,
        'btn_label': 'Go Go GO',
        'title': 'Registration',
        'pass': 'qZcP214zHFnhJvINkbks_   - pass for tests',
    }
    return render(request, 'accounts/auth.html', context)
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import MyUser


class MyUserChangeForm(UserChangeForm):
    class Meta:
        model = MyUser
        fields = ['username', 'email']

class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = MyUser
        fields = ['username']

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {ProfileBadgeComponent} from './profiles'
import {TweetСomponent, TweetDetailComponent, FeedTweetСomponent} from './tweets';
import * as serviceWorker from './serviceWorker';

const tweetsEl =  document.getElementById('tweetme-2')
const tweetsFeedEl =  document.getElementById('tweetme-2-feed')
const tweetsElDetail =   document.getElementById('tweetme-2-detail')
const userProfilesElDetail =    document.getElementById('tweetme-2-user-profile')
const appEl = null // document.getElementById('root')

const e = React.createElement

if (tweetsFeedEl) {
  
  ReactDOM.render(
    e(FeedTweetСomponent, tweetsFeedEl.dataset), tweetsFeedEl );
  }

if (appEl) {
  ReactDOM.render(<App />, appEl );
};

if (tweetsEl) {
  // 
  ReactDOM.render(
    e(TweetСomponent, tweetsEl.dataset), tweetsEl );
  }
  // 
if (tweetsElDetail) {
  ReactDOM.render(
  e(TweetDetailComponent, tweetsElDetail.dataset), tweetsElDetail );
}
if (userProfilesElDetail) {
    ReactDOM.render(
    e(ProfileBadgeComponent, userProfilesElDetail.dataset), userProfilesElDetail );
  }


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

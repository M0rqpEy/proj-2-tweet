import React, {useEffect, useState} from 'react'
import {UserDisplay, UserPicture, DisplayNumber} from './components'
import {apiProfileDetail, apiProfileActionFollow} from './lookup'





const ProfileBadge = (props) => {
    const {profile, didFollowAction} = props
    const [buttonName, setButtonName] = useState(profile.is_following ? 'Unfollow' : 'Follow')
    const className = buttonName === 'Unfollow'
                        ? 'btn btn-danger'
                        : 'btn btn-success'
    const buttonFollowHandler = (event) => {
        event.preventDefault();
        didFollowAction(buttonName)
        setButtonName(buttonName ==='Follow' ? 'Unfollow' : 'Follow')

    }                    
    return  <div> 
                 <p> <UserPicture user={profile} hide /> </p>
                
                <p><UserDisplay user={profile} includeName  hide /></p>
                <p><DisplayNumber>{profile.follower_count}</DisplayNumber> 
                    {profile.follower_count === 1 ? " folower" : " followers"}</p>
                <p><DisplayNumber>{profile.following_count}</DisplayNumber> following</p>
                <p>{profile.location}</p>
                <p>{profile.bio}</p>
                { profile.username !== profile.request_user_username
                ?<button 
                    onClick={buttonFollowHandler} 
                    className={className}>
                        {buttonName}
                </button>
                :null
                }
                
            </div> 
}



export const ProfileBadgeComponent = (props) => {
    const {username} = props
    const [didLookup, setDidLookup] = useState(false)
    const [profile, setProfile] = useState(null)
    useEffect(()=>{
        if (!didLookup) {
            apiProfileDetail(username, (response, status) => {
                if (status === 200) {
                    setDidLookup(true)
                    setProfile(response)
                }
            })
        }
    },[username, profile,didLookup])

    const didFollowAction = (action)=>{
        apiProfileActionFollow(username, action.toLowerCase(), (response, status) => {
            if (status === 200) {
                setDidLookup(false)
            }
        })
    }
    return  <>
                {profile === null 
                ? <p>Loading...</p> 
                :  <ProfileBadge 
                    profile={profile}  
                    didFollowAction={didFollowAction}
                   /> }
            </>
}
import React  from 'react';
import numeral from 'numeraljs'


export const UserDisplay = (props) => {
  const {user, includeName, hide} = props
  const displayName = includeName ? `${user.first_name} ${user.last_name} ` : null 
  
  return <>
        {displayName}
        {!hide 
          ?<UserLink username={user.username}> @{user.username} </UserLink>
          : `@${user.username}`}
        </>
}


export const UserPicture = (props) => {
  const {user, hide} = props
  const spanPicture = <span className="mx-1 px-3 py-2 rounded-circle bg-dark text-white">
                        {user.username[0]}
                      </span>            
  return  !hide 
          ? <UserLink username={user.username} > {spanPicture}</UserLink>
          : spanPicture
        
};

export const UserLink = (props) => {
  const {username} = props
  const userLinkHandler = (event) => {
    window.location.href = `/profiles/${username}/detail/`
  }
  return  <span className='pointer' onClick={userLinkHandler}>
            {props.children}
          </span>
       
}


export const DisplayNumber = (props) =>{
  const format = numeral(props.children).value() > 1000 ? '0.0a': '0a'
  return <span >{numeral(props.children).format(format)}</span>
}
import {backendLookUp} from '../loaddata';


export function apiProfileDetail(username, callback) {
  
    backendLookUp('GET', `/profiles/${username}`, callback)
  };

  export function apiProfileActionFollow(username, action, callback){
    let data = {"action":action}
    backendLookUp('POST', `/profile/${username}/follow/`, callback, data);
  };
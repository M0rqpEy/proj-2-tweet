import {backendLookUp} from '../loaddata';
 


// GET
export function apiFeedTweetList(callback, nextUrl) {
  let endpoint = '/tweets/feed/'

  if (nextUrl !== null && nextUrl !== undefined) {
    // endpoint = nextUrl.replace('http://127.0.0.1:8000/api', '')
    endpoint = nextUrl.replace('https://tweetme-3.herokuapp.com/api', '')
  }

  backendLookUp('GET', endpoint, callback)
};




export function apiTweetList(username, callback, nextUrl) {
    let endpoint = '/tweets/'
    if (username){
       endpoint = `/tweets/?username=${username}`
    }
    if (nextUrl !== null && nextUrl !== undefined) {
      // endpoint = nextUrl.replace('http://127.0.0.1:8000/api', '')
      endpoint = nextUrl.replace('https://tweetme-3.herokuapp.com/api', '')

    }

    backendLookUp('GET', endpoint, callback)
};



export function apiTweetDetail(tweetId, callback) {
  
  backendLookUp('GET', `/tweets/${tweetId}`, callback)
};



export function apiTweetDelete(tweetId, callback) {
  
  backendLookUp('POST', `/tweets/${tweetId}/delete/`, callback)
};




// POST
export function apiTweetActionLike(tweet, callback){
  tweet['action'] = 'like';
  backendLookUp('POST', '/tweets/action/', callback, tweet);
};

export function apiTweetActionDisLike(tweet, callback){
  tweet['action'] = 'unlike';
  backendLookUp('POST', '/tweets/action/', callback, tweet);
};

export function apiTweetActionRetweet(tweet, callback){
  tweet['action'] = 'retweet';
  backendLookUp('POST', '/tweets/action/', callback, tweet);
};

export function apiTweetCreate(newTweet, callback){
  backendLookUp('POST', '/tweets/create/', callback, {content: newTweet});
};
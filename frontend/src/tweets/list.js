import React, {useState, useEffect}   from 'react';
import {Tweet} from './detail'
import {apiTweetList} from './lookup';


export function TweetList(props) {
    const {newTweet } = props // new tweet from create form
    const [nextUrl, setNextUrl] = useState(null)
    const [retweet, setRetweet] = useState([])  //list tweet from api
    const [tweets, setTweets] = useState([])  // finish tweets list
    const [tweetsDidSet, setTweetsDidSet] = useState(false)  // state for load data 

    useEffect(()=>{
      // create action
    if (([...newTweet].concat(tweets)).length !== tweets.length) {
      setTweetsDidSet(false)
      setTweets([...newTweet].concat(tweets))

    }
    },[newTweet, setTweetsDidSet])

    useEffect(()=>{
      // retweet action
    if (([...retweet].concat(tweets)).length !== tweets.length) {
        setTweets([...retweet].concat(tweets))
      }
      
    },[retweet])
    
    useEffect(()=>{
      // load tweets from api
      const handleTweetListlookup = function(response, status){
        if (!tweetsDidSet ) {
          if (status === 200) {
            setNextUrl(response.next)
            setTweets(response.results)
            setTweetsDidSet(true)
          } 
        };
      }
      apiTweetList(props.username, handleTweetListlookup);
    },[setTweets, props.username, tweetsDidSet])
    
    
    const handleSetTweetsDidSet = (retweetTweet)=>{
      setRetweet([retweetTweet])
    }

    const handleDeleteTweets = (response)=>{
        if (response) {
          setTweetsDidSet(false)
        }
    } 

    const handleNextUrl = (event)=>{
      event.preventDefault();
      const handleLoadNextPage = (response, status)=>{
          if (status === 200) {
            setNextUrl(response.next)
            setTweets([...tweets].concat(response.results))
          }  
        };
      
      apiTweetList(props.username, handleLoadNextPage, nextUrl)
    }
  return <React.Fragment>
  {tweets.map((item, index)=>{
    return <Tweet 
    handleDeleteTweets={handleDeleteTweets}               // handle delete
    handleSetTweetsDidSet={handleSetTweetsDidSet}   // handle reetweet
    tweet={item} 
    className='col-10 mx-auto mb-5 border p-1 bg-white text-dark' 
    key={`${index} - {tweet.id} `} />  
  })} 
  { nextUrl !== null && <button onClick={handleNextUrl} className='btn btn-outline-success'>Load next</button> }
  </React.Fragment>    
  };  
import React  from 'react';
import {ActionButton, RetweetButton, DeleteButton} from './button'
import {UserDisplay, UserPicture} from '../profiles'


function ParentTweet(props){
    const {tweet} = props
    return tweet.org_tweet 
      ? <Tweet isRetweet 
          retweeter={props.retweeter} 
          hideButton 
          classname={' '}
          isDetailView 
          tweet={tweet.org_tweet} />
      : null
  }
  
export function Tweet(props){
      const {tweet, hideButton,handleSetTweetsDidSet, isRetweet, handleDeleteTweets} = props
      let className = props.className ? props.className : 'mx-auto'
      className = isRetweet === true ? `${className} border rounded p-2` : className
      const handleViewDetailTweet = (event)=>{
        event.preventDefault();
        window.location.href = `/${tweet.id}`

      };

      return <div className={className}>
              {isRetweet === true && <div className="mb-2">
                                      <span className='small text-muted'>
                                        Retweet via <UserDisplay user={props.retweeter} />
                                      </span> 
                                    </div>}
        <div className="d-flex ">
          <div className='mt-2'>
            <UserPicture user={tweet.user}/>
          </div>
        <div className="col-11">
         <div>
          <p>
            
            <UserDisplay user={tweet.user} includeName />
          </p>
          <p>{tweet.content}</p>
                <ParentTweet tweet={tweet} retweeter={tweet.user} />
        </div>
        <div className="btn btn-group mt-1" >
        {!hideButton &&  <React.Fragment>
        <ActionButton tweet={tweet} 
                      action={{type:'likeUnlike' }}/>
        <RetweetButton tweet={tweet}
                      handleSetTweetsDidSet={handleSetTweetsDidSet}
                      action={{type:'retweet', display:'Retweet'}} 
                      className='btn btn-outline-info btn-sm mx-1'/>
        { tweet.is_owner &&
        <DeleteButton tweet={tweet}
                      isDetailView={props.isDetailView}
                      handleDeleteTweets={handleDeleteTweets}
                      className='btn btn-outline-danger btn-sm mx-1'/>}
        </React.Fragment>}
       
       { !props.isDetailView && <React.Fragment>
        <button onClick={handleViewDetailTweet} 
                className='btn btn-outline-primary btn-sm'>View</button>
       </React.Fragment>}
        </div> 
        </div>
      </div>  
      </div>
    };
import React, {useState, useEffect}   from 'react';
import {TweetList} from './list'
import {TweetCreate} from './create'
import {Tweet} from './detail'
import {FeedTweetList} from './feed'
import {apiTweetDetail} from './lookup'




export function FeedTweetСomponent(props){
  const [newTweet, setNewTweet] = useState([])
  const canCreate = props.canCreate === 'false' ? false : true  // data-can-create='false'

  const handleTweetCreate = (createdTweet) => {
    setNewTweet(createdTweet)
    };
  return <div className={props.className}>
      { canCreate &&  <TweetCreate {...props} handleTweetCreate={handleTweetCreate}/>}
      <FeedTweetList {...props} 
                newTweet={newTweet}/>
  </div>
};


export function TweetСomponent(props){
  const [newTweet, setNewTweet] = useState([])
  const canCreate = props.canCreate === 'false' ? false : true  // data-can-create='false'

  const handleTweetCreate = (createdTweet) => {
    setNewTweet(createdTweet)
    };
  return <div className={props.className}>
      { canCreate &&  <TweetCreate {...props} handleTweetCreate={handleTweetCreate}/>}
      <TweetList {...props} 
                newTweet={newTweet}/>
  </div>
};


export function TweetDetailComponent(props){
  
  const {tweetId} = props
  const [tweet, setTweet] = useState(null)
  const [didLoadTweet, setDidLoadTweet] = useState(false)
  useEffect(()=>{
    if (!didLoadTweet) {
      apiTweetDetail(tweetId, (response, status)=>{
        if (status === 200) {
          setTweet(response)
        } 
      })

      setDidLoadTweet(true)
    }
  },[tweetId,didLoadTweet, setDidLoadTweet])
  return tweet === null ? null: <Tweet isDetailView 
                                      tweet={tweet} 
                                      className={props.className}/>


};
  
  





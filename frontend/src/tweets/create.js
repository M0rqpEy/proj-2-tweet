import React, {useState, useEffect} from 'react';
import { apiTweetCreate } from './lookup';

const Error = (props) => {
  const {error} = props
  return <>{error !== null
    ? <div className="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Ouuuuch</strong> {props.error.detail.content}
            <button onClick={props.closeBlockError} className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
      </div>
    : null}</>
          
};




export function TweetCreate(props) {
    const [error, setError] = useState(null)            // value error
    const textInput = React.createRef();
    const [occurError, setOccurError] = useState(false)  // bollean 
    useEffect(()=>{
      if (occurError && error !== null) {
        console.log(error)
      textInput.current.value = error.data.content;
      }
    },[occurError, error])

    const handleSubmit = (event) => {
      event.preventDefault();
      apiTweetCreate(textInput.current.value, (response, status)=>{
    
        if (status === 201) {
          props.handleTweetCreate([response])
          setOccurError(false)
        }else{
          setOccurError(true)
          setError(response)
        };
      });
      textInput.current.value = '';
    };

    const closeBlockError = ()=> {
      setOccurError(null)
    }
    
    return  <> {occurError  ? <Error error={error} closeBlockError={closeBlockError}/> : null }
         <div className='col-10 mx-auto mb-5 mt-2'>
          <form onSubmit={handleSubmit} >
            <textarea  className="form-control" ref={textInput} required={true} >
            </textarea>
            <div className="btn-group">
            <button className='btn btn-success my-1'>Tweet</button>
            <button onClick={()=>{textInput.current.value = ''}} 
                    className='btn btn-info my-1 mx-1'>
              Cancel
            </button>
            </div>
            </form>
          </div>
          </>
  };
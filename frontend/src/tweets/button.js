import React, {useState}   from 'react';
import {
    apiTweetActionRetweet, 
    apiTweetActionLike, 
    apiTweetActionDisLike,
    apiTweetDelete} from './lookup';



export function ActionButton(props){
    const {tweet, action} = props
    const [likes, setLikes] = useState(tweet.likes ? tweet.likes : 0 ) // quantity
    const [userLike, setUserLike] = useState(tweet.is_liked)           // boolean
    const className = props.className ? props.className : 'btn btn-primary btn-sm'
    const actionDisplay =  userLike ? `${likes} Dislike` : `${likes} Like`
    
    const handleClick = (event)=>{
      event.preventDefault();

      if (action.type === 'likeUnlike') {
        if (userLike) {
          apiTweetActionDisLike(tweet, (response, status)=>{
            if (status === 200) {
              setLikes(response.likes)
              setUserLike(false)
            } 
          })
        } else {
            apiTweetActionLike(tweet, (response, status)=>{
              if (status === 200) {
                setLikes(response.likes)
                setUserLike(true)
              }
            } 
            )
          };
      }

      };
    return <button onClick={handleClick} className={className}>{actionDisplay}</button>     
  };



  export function RetweetButton(props){
    const {tweet, action, handleSetTweetsDidSet} = props
    const className = props.className ? props.className : 'btn btn-primary btn-sm'
    
    const handleClick = (event)=>{
      event.preventDefault();
      
        apiTweetActionRetweet(tweet, (response, status)=>{
          if (status === 201 ) {
            if (handleSetTweetsDidSet){
              handleSetTweetsDidSet(response);
            }
          }
        })
      
      };
    return <button onClick={handleClick} className={className}>{action.display}</button>     
  };

  export function DeleteButton(props){
    const {tweet,handleDeleteTweets, isDetailView } = props
    const className = props.className ? props.className : 'btn btn-danger btn-sm'
    
    const handleClick = (event)=>{
      event.preventDefault();
      
        apiTweetDelete(tweet.id, (response, status)=>{
              if (status === 200) {
                if (isDetailView) {
                  window.location.href='/'
                }else {
                  handleDeleteTweets(true)
                }
              }   
         
        })
      };
    return <button onClick={handleClick} className={className}>Delete</button>     
  };
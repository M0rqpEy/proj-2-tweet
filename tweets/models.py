import random
from django.db import models
from django.db.models import Q
from django.contrib.auth import get_user_model

User = get_user_model()


class TweetQuerySet(models.QuerySet):
    
    def feed(self, user):
        profiles_exist = user.following.exists()
        followed_user_id = []
        if profiles_exist:
            followed_user_id = user.following.values_list('user_id', flat=True)
        return self.filter(
                        Q(user_id__in=followed_user_id) 
                        | Q(user=user)
                    ).distinct()

class TweetManager(models.Manager):
    
    def get_queryset(self):
        return TweetQuerySet(self.model, self._db)
    
    def feed(self, user):
        return self.get_queryset().feed(user)

class TweetLikes(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tweet = models.ForeignKey('Tweet', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'tweet')


class Tweet(models.Model):
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    content = models.TextField(blank=True, null=True)
    image = models.FileField(upload_to='images/', blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, 
                                    related_name='tweet_user', 
                                    blank=True, 
                                    through='TweetLikes')
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = TweetManager()

    class Meta:
        ordering = ('-id',)
    
    def __str__(self):
        return self.content[:15]

    # @property
    # def is_retweet(self):
    #     return self.parent != None
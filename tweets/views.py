from django.http import JsonResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.conf import settings
from django.contrib.auth import get_user_model

from .models import Tweet


def tweets_list_view(request):
    return render(request, 'tweets/list.html')


def tweets_feed_view(request):
    if not request.user.is_authenticated:
        return redirect(reverse('tweet_list'))
    return render(request, 'tweets/feed.html')



def tweets_detail_view(request, tweet_id):
    get_object_or_404(Tweet, id=tweet_id)
    return render(request, 'tweets/detail.html', {'tweet_id':tweet_id})


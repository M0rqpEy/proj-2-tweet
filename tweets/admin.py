from django.contrib import admin

from .models import Tweet, TweetLikes

class TweetLikesAdmin(admin.TabularInline):
    model = TweetLikes

@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
    inlines = [TweetLikesAdmin]
    search_fields = ['user__username', 'content']
    list_display = ['id','__str__', 'user']

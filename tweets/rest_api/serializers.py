from django.conf import settings
from rest_framework import serializers

from profiles.rest_api.serializers import PublicProfileSerializer
from ..models import Tweet

MAX_TWEET_LENGTH = settings.MAX_TWEET_LENGTH
MIN_TWEET_LENGTH = settings.MIN_TWEET_LENGTH
TWEET_VALID_ACTION = settings.TWEET_VALID_ACTION


class TweetActionSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    action = serializers.CharField()
    content = serializers.CharField(allow_blank=True, required=False)

    def validate_action(self, value):
        value = value.lower().strip() # 'Like ' -> 'like'
        if not value in TWEET_VALID_ACTION:
            raise serializers.ValidationError('This is not valid tweet action')
        return value
    
    



class TweetCreateSerializer(serializers.ModelSerializer):
    user = PublicProfileSerializer(source='user.profile',read_only=True)
    likes = serializers.SerializerMethodField(read_only=True)
    timestamp = serializers.SerializerMethodField(read_only=True)

    
    class Meta:
        model = Tweet
        fields = ['user', 'id', 'content', 'likes','timestamp']

    def get_likes(self, obj):
        return obj.likes.count()
    
   
    def get_timestamp(self, obj):
        return obj.timestamp.strftime('%x - %X')
    
    def validate_content(self, value):
        if len(value) > MAX_TWEET_LENGTH:
            raise serializers.ValidationError('This tweet to long')
        if len(value) < MIN_TWEET_LENGTH:
            raise serializers.ValidationError('This tweet to short')
        return value

    


class TweetSerializer(serializers.ModelSerializer):
    user = PublicProfileSerializer(source='user.profile',read_only=True)
    likes = serializers.SerializerMethodField(read_only=True)
    is_owner = serializers.SerializerMethodField(read_only=True)
    is_liked = serializers.SerializerMethodField(read_only=True)
    org_tweet = TweetCreateSerializer(source='parent', read_only=True)
    timestamp = serializers.SerializerMethodField(read_only=True)



    class Meta:
        model = Tweet
        fields = [
            'user',
            'is_owner',
            'id', 
            'content',
            'likes',
            'is_liked', 
            'org_tweet',
            'timestamp'
        ]

    def get_likes(self, obj):
        return obj.likes.count()
    
    def get_is_liked(self, obj):
        request = self.context.get('request')
        return  request.user in obj.likes.all()

    def get_is_owner(self, obj):
        request = self.context.get('request')
        return  request.user == obj.user
    

    def get_timestamp(self, obj):
        return obj.timestamp.strftime('%x - %X')

    
from django.urls import path 

from .views import (
    tweet_create,
    tweet_action,
    tweet_feed_page,
    tweet_page, 
    tweet_list, 
    tweet_delete
    )

urlpatterns = [
    path('action/', tweet_action),
    path('create/', tweet_create),
    path('<int:tweet_id>/', tweet_page),
    path('<int:tweet_id>/delete/', tweet_delete),
    path('feed/', tweet_feed_page),
    path('', tweet_list),
]

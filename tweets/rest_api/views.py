import random
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils.http import is_safe_url
from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from ..forms import TweetForm
from ..models import Tweet
from profiles.models import Profile, FollowerRelation
from .serializers import (
    TweetSerializer,
    TweetActionSerializer,
    TweetCreateSerializer)

USER = get_user_model().objects.first()
ALLOWED_HOSTS = settings.ALLOWED_HOSTS
TWEETS_PER_PAGE = settings.TWEETS_PER_PAGE

def tweet_page_pure_django(request, tweet_id):
    data = {}
    status = 200
    try:
        tweet = Tweet.objects.get(id=tweet_id)
        data['id'] = tweet.id
        data['content'] = tweet.content
    except:
        data['message'] = f"we haven't tweet with id#{tweet_id}"
        status = 404
    return JsonResponse(data, status=status)


def tweet_list_pure_django(request):
    qs = Tweet.objects.all()
    tweet_list = [x.serialize() for x in qs]
    data = {
        'isUser': False,
        'response': tweet_list
    }
    return JsonResponse(data)


def tweet_create_pure_django(request, *args, **kwargs):
    form = TweetForm(request.POST or None)
    next_url = request.POST.get('next') or None
    if form.is_valid():
        tweet = form.save(commit=False)
        tweet.save()
        if request.is_ajax():
            return JsonResponse(tweet.serialize(), status=201)
        if next_url != None and is_safe_url(next_url, ALLOWED_HOSTS):
            return redirect(next_url)
        form = TweetForm()
    if form.errors :
        if request.is_ajax():
            return JsonResponse(form.errors, status=400)
    return render(request, 'components/form.html', {'form':form})


def get_paginator(qs, request):
    paginator = PageNumberPagination()
    paginator.page_size = TWEETS_PER_PAGE
    paginator_qs = paginator.paginate_queryset(qs, request)
    serializer = TweetSerializer(paginator_qs, many=True, context={'request': request})
    return paginator.get_paginated_response(serializer.data )

#GET
from django.db.models import Prefetch
@api_view(['GET'])
def tweet_list(request):
    qs = Tweet.objects.all()
    username = request.GET.get('username')
    if username:
        qs = qs.filter(user__username__iexact=username)
    qs = qs.select_related(
                        'user', 
                        'user__profile',
                        'parent__user', 
                        'parent__user__profile',
                        ).prefetch_related(
                                                        'likes', 
                                                        'parent__likes',
                                                        'user__following',
                                                        'user__profile__followers',
                                                        'parent__user__following',
                                                        'parent__user__profile__followers',
                                                        )
    return get_paginator(qs, request)
    # serializer = TweetSer

@api_view(['GET'])
def tweet_page(request, tweet_id):
    '''
    tweet detail page
    '''
    qs = Tweet.objects.filter(id=tweet_id)
    if not qs.exists():
        return Response({'detail': 'this tweet not exist'}, status=404)
    obj = qs.first()
    serializer = TweetSerializer(obj, context={'request':request})
    return Response(serializer.data)


# POST
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def tweet_create(request, *args, **kwargs):
    serializer = TweetCreateSerializer(data=request.data, context={'request':request})
    if serializer.is_valid():
        serializer.save(user=request.user)
        return Response(serializer.data , status=201)
    data = {
    "detail" : serializer.errors,
    "data": request.data

    }
    return Response(data, status=400)


@api_view(['DELETE', 'POST'])
@permission_classes([IsAuthenticated])
def tweet_delete(request, tweet_id):
    qs = Tweet.objects.filter(id=tweet_id)
    if not qs.exists():
        return Response({'detail': 'this tweet not exist'}, status=404)
    qs = qs.filter(user=request.user)
    if not qs.exists():
        return Response({'detail': 'U cannt delete this tweet'}, status=403)
    obj = qs.first()
    obj.delete()
    return Response({'detail': 'Tweet\'s deleted'}, status=200)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def tweet_action(request, *args, **kwargs):
    '''
    id required!
    valid actions: like, unlike, retweet
    '''
    serializer = TweetActionSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        tweet_id = serializer.validated_data.get('id')
        action = serializer.validated_data.get('action')
        content = serializer.validated_data.get('content')
        qs = Tweet.objects.filter(id=tweet_id)
        if not qs.exists():
            return Response({'detail': 'this tweet not exist'}, status=404)
        obj = qs.first()
        if action == 'like':
            obj.likes.add(request.user)
            serializer = TweetSerializer(obj, context={'request':request})
            return Response(serializer.data, status=200)
        if action == 'unlike':
            obj.likes.remove(request.user)
            serializer = TweetSerializer(obj, context={'request':request})
            return Response(serializer.data, status=200)
        if action == 'retweet':
            new_tweet = Tweet.objects.create(
                user=request.user,
                parent=obj,
                content=f'Retweet tweet#{obj.id}'
            )
            print(new_tweet)
            serializer = TweetSerializer(new_tweet, context={'request':request})
            return Response(serializer.data, status=201)
    return Response({'detail': 'This tweet action is not valid'}, status=400) 


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def tweet_feed_page(request):
    user = request.user
    qs = Tweet.objects.feed(user).order_by('-timestamp')
    qs = qs.select_related(
            'user', 
            'parent__user', 
            'user__profile',
            'parent__user__profile',
            ).prefetch_related(
                'likes', 
                'parent__likes',
                'user__profile__followers',
                'parent__user__profile__followers',
                'user__following',
                'parent__user__following',
            )
    return get_paginator(qs, request)
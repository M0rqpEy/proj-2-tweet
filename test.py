class LenError(BaseException):
    pass

class NickName():
    def __init__(self):
        self._name = None
    
    @property
    def name(self):
        return self._name
    
    @name.setter
    def set_name(self, value):
        if len(value) < 4:
            raise LenError("Too short name, bitcha")
        self._name = value

if __name__=='__main__':
    n1 = NickName()
    n1.set_name = 'qw2e'
    print(n1.name)    